const { join } = require("path");
const { validationResult } = require("express-validator");
const { logDailyName, writeToLogFile } = require("write-logs-js");

const erroLog = logDailyName(`${join(__dirname, "../../../")}/logs`, "error");

module.exports = {
  validateReq(validations) {
    return async (req, res, next) => {
      await Promise.all(validations.map((validation) => validation.run(req)));

      const errors = validationResult(req);
      if (errors.isEmpty()) {
        return next();
      }

      writeToLogFile(JSON.stringify(errors.array()), erroLog);
      res.status(400).send({
        retorno: 400,
        mensaje: "Los parametros enviados son incorrectos",
        objeto: null
      });
    };
  }
};
