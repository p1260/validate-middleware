const crypto = require("crypto");

require("dotenv").config();
const keysalt = process.env.KEY_SALT || "1234";
const keysecret = process.env.KEY_SECRET || "s3cr3tk3y";

module.exports = {
  async isTokenValid(req, res, next) {
    var key = crypto
      .createHmac("sha256", keysalt)
      .update(keysecret, "utf8")
      .digest("hex");

    if (key === req.headers["x-api-key"]) {
      return await next();
    } else {
      return await res
        .status(400)
        .send({ retorno: 400, mensaje: "Token invalido.", objeto: null });
    }
  },

  async isJsonContentype(req, res, next) {
    if (req.headers["content-type"] !== "application/json") {
      return await res
        .status(400)
        .send({ retorno: 400, mensaje: "Permiso denegado.", objeto: null });
    } else {
      return await next();
    }
  }
};
