const { join } = require("path");
const { logDailyName, writeToLogFile } = require("write-logs-js");
const { ValidationError } = require("express-json-validator-middleware");

const erroLog = logDailyName(`${join(__dirname, "../../../")}/logs`, "error");

module.exports = {
  async logErrors(err, req, res, next) {
    if (err instanceof ValidationError) {
      writeToLogFile(JSON.stringify(err.validationErrors), erroLog);
      // Handle the error
      return await res.status(400).send({
        retorno: 400,
        mensaje: "Los parametros enviados son incorrectos",
        objeto: null
      });
    } else {
      writeToLogFile(err, erroLog);

      return await res.status(500).send({
        retorno: 500,
        mensaje: "No se puede obtener la información solicitada.",
        objeto: null
      });
    }
  },

  async enableCors(req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,POST");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, contentType, Content-Type, Accept, Authorization"
    );

    if (req.method === "OPTIONS") {
      return await res.send(200);
    } else {
      return await next();
    }
  }
};
