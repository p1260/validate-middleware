const httpMocks = require("node-mocks-http");

const { isJsonContentype, isTokenValid } = require("../middleware/auth");

describe("middleware auth", () => {
  test("is token valido", async () => {
    const mockReq = httpMocks.createRequest();
    const mockRes = httpMocks.createResponse();

    await isTokenValid(mockReq, mockRes);

    expect(mockRes.statusCode).toEqual(400);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getData()).toEqual({
      retorno: 400,
      mensaje: "Token invalido.",
      objeto: null
    });
  });

  test("is json conten-type", async () => {
    const mockReq = httpMocks.createRequest();
    const mockRes = httpMocks.createResponse();

    await isJsonContentype(mockReq, mockRes);

    expect(mockRes.statusCode).toEqual(400);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getData()).toEqual({
      retorno: 400,
      mensaje: "Permiso denegado.",
      objeto: null
    });
  });
});
