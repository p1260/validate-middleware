const httpMocks = require("node-mocks-http");
const { checkSchema } = require("express-validator");

const { validateReq } = require("../middleware/validator_input_parameter");
const { vaLogin, vaLoginBody } = require("./validator");

describe("middleware input parameters", () => {
  const _user = {
    UsuarioId: "lbarrera",
    Password: "MTIz",
    SistemaId: "DICEA",
    Asunto: "123456ORM12020FM",
    Orden: [12],
    Asuntos: [
      {
        Clase: "Probar"
      },
      { Clase: "test" }
    ]
  };
  let next = jest.fn();

  test("valid request parameters schema", async () => {
    const mockReq = httpMocks.createRequest({
      body: _user
    });
    const mockRes = httpMocks.createResponse();

    await validateReq(checkSchema(vaLogin))(mockReq, mockRes, next);

    expect(mockRes.statusCode).toEqual(200);
    expect(mockRes.statusMessage).toEqual("OK");
  });

  test("valid request parameters body", async () => {
    const mockReq = httpMocks.createRequest({
      body: _user
    });
    const mockRes = httpMocks.createResponse();

    await validateReq(vaLoginBody)(mockReq, mockRes, next);

    expect(mockRes.statusCode).toEqual(200);
    expect(mockRes.statusMessage).toEqual("OK");
  });

  test("ainvalid request parameters schema", async () => {
    const mockReq = httpMocks.createRequest();
    const mockRes = httpMocks.createResponse();

    await validateReq(checkSchema(vaLogin))(mockReq, mockRes, next);

    expect(mockRes.statusCode).toEqual(400);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getData()).toEqual({
      retorno: 400,
      mensaje: "Los parametros enviados son incorrectos",
      objeto: null
    });
  });

  test("invalid request parameters body", async () => {
    const mockReq = httpMocks.createRequest();
    const mockRes = httpMocks.createResponse();

    await validateReq(vaLoginBody)(mockReq, mockRes, next);

    expect(mockRes.statusCode).toEqual(400);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getData()).toEqual({
      retorno: 400,
      mensaje: "Los parametros enviados son incorrectos",
      objeto: null
    });
  });
});
