const { body } = require("express-validator");

module.exports = {
  vaLogin: {
    UsuarioId: {
      trim: true,
      notEmpty: true
    },
    Password: {
      trim: true,
      notEmpty: true,
      isBase64: true
    },
    SistemaId: {
      trim: true,
      notEmpty: true
    },
    Asunto: {
      trim: true,
      notEmpty: true,
      matches: { options: /[0-9]{6}....[0-9]{4}[aA-zZ]{2}/g }
    },
    Orden: {
      isArray: true,
      notEmpty: true
    },
    "Asuntos.*.Clase": {
      trim: true,
      notEmpty: true
    }
  },
  vaLoginBody: [
    body("UsuarioId").notEmpty().trim(),
    body("Password").notEmpty().trim().isBase64(),
    body("SistemaId").notEmpty().trim(),
    body("Asunto")
      .notEmpty()
      .trim()
      .matches(/[0-9]{6}....[0-9]{4}[aA-zZ]{2}/),
    body("Orden").notEmpty().isArray()
  ]
};
