const httpMocks = require("node-mocks-http");
const { ValidationError } = require("express-json-validator-middleware");

const { enableCors, logErrors } = require("../middleware/exception");

describe("middleware exception", () => {
  test("logs errors parametros", async () => {
    const mockReq = httpMocks.createRequest();
    const mockRes = httpMocks.createResponse();

    await logErrors(new ValidationError(), mockReq, mockRes);

    expect(mockRes.statusCode).toEqual(400);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getData()).toEqual({
      retorno: 400,
      mensaje: "Los parametros enviados son incorrectos",
      objeto: null
    });
  });

  test("logs errors exeception", async () => {
    const mockReq = httpMocks.createRequest();
    const mockRes = httpMocks.createResponse();

    await logErrors({ error: "probando" }, mockReq, mockRes);

    expect(mockRes.statusCode).toEqual(500);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getData()).toEqual({
      retorno: 500,
      mensaje: "No se puede obtener la información solicitada.",
      objeto: null
    });
  });

  test("enable cors", async () => {
    const mockReq = httpMocks.createRequest({
      method: "OPTIONS"
    });
    const mockRes = httpMocks.createResponse();

    await enableCors(mockReq, mockRes);

    expect(mockRes.statusCode).toEqual(200);
    expect(mockRes.statusMessage).toEqual("OK");
    expect(mockRes._getHeaders()).toEqual({
      "access-control-allow-origin": "*",
      "access-control-allow-methods": "GET,HEAD,POST",
      "access-control-allow-headers":
        "Origin, X-Requested-With, contentType, Content-Type, Accept, Authorization"
    });
  });
});
